import service from '../services/hello';

export default () => {
    return service.invoke();
}