import service from '../services/greeting';

export default () => {
    return service.invoke();
}