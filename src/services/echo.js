/**
 * echo service
 */
export default {
    request: {
        method: 'POST',
        params: {
            message: String
        }
    },
    
    invoke: async (message) => {
        const response = { echo: message };
        return response;
    }
}