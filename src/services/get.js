/**
 * get service
 */
export default {
    request: {
        path: '/:id/test/:test',
        method: 'GET',
        params: {
            id: String,
            test: String
        }
    },
    
    invoke: async (id, test) => {
        const response = { id: id, test: test };
        return response;
    }
}