import hello from '../facades/hello';
import greeting from '../facades/greeting';

export default {
    request: {
        method: 'GET'
    },
    
    invoke: async () => {
        const h = await hello();
        const g = await greeting();
    
        const response = { hello: h, greeting: g };
        return response;
    }
}
