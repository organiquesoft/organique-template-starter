/**
 * hello service
 */
export default {
    request: {
        method: 'GET',
        params: {
            q: String
        }
    },
    
    invoke: async (q) => {
        const response = { message: 'Hello', q: q };
        return response;
    }
}
