/**
 * Server, service endpoint
 */
import Express from 'express';
import BodyParser from 'body-parser';
import glob from 'glob';
import _ from 'lodash';
import Handler from './request-handler';

const startPort = 3000;
const rootPath = process.cwd();

// new server instance
const server = new Express();

server.use(BodyParser.json());
server.use(BodyParser.urlencoded({
    extended: true
}));

// scan services
const files = glob.sync(`${rootPath}/src/services/**/*.js`);
_.forEach(files, (file) => {
    const service = require(file).default;
    let path = file.replace(`${rootPath}/src/services`, '').replace('.js', '');
    if (path === '/index') {
        path = '/';
    }

    if (service.request && service.invoke) {
        if (service.request.path) {
            path += service.request.path;
        }

        if (service.request.method.toUpperCase() === 'GET') {
            console.log(`[GET] ${path}`);
            server.get(path, Handler(service));
        } else if (service.request.method.toUpperCase() === 'POST') {
            console.log(`[POST] ${path}`);
            server.post(path, Handler(service));
        } else if (service.request.method.toUpperCase() === 'PUT') {
            console.log(`[PUT] ${path}`);
            server.put(path, Handler(service));
        } else if (service.request.method.toUpperCase() === 'DELETE') {
            console.log(`[DELETE] ${path}`);
            server.delete(path, Handler(service));
        }
    }
});

// start server
server.listen(3000, () => {
    console.log(`Server listening on port 3000!`);
});
