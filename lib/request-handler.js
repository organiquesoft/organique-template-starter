import _ from 'lodash';

export default (service) => {
    
    const handler = (req, res) => {
        const args = [];
        for (const p in service.request.params) {
            let v = null;
            v = req.params[p];
            if (!v) {
                v = req.body[p];
            }
            if (!v) {
                v = req.query[p];
            }

            args.push(v);
        }

        const invoke = async () => {
            try {
                const data = await service.invoke.apply(null, args);
                res.json({
                    success: true,
                    data
                });
            } catch (err) {
                //TODO handler error
                res.status(500).send({ 
                    success: false,
                    error: {
                        message: err.message
                    }
                 });
            }
        }

        invoke();

    }

    return handler;
}